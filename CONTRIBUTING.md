## Setting Up Your Development Environment

Before you start, make sure you have Python and Git installed on your system. Our project uses `pre-commit` to manage various linters and formatters, so you'll need to install it as well.

1. **Install Pre-commit**

   If you haven't already, install `pre-commit` globally with pip:

   ```sh
   pip install pre-commit
   ```

2. **Clone the Repository**

   If you haven't cloned the project repository yet, do so by running:

   ```sh
   git clone <repository-url>
   cd <repository-directory>
   ```

   Replace `<repository-url>` and `<repository-directory>` with the actual URL and name of the project repository.

3. **Install the Pre-commit Hooks**

   Navigate to the root of the project directory and install the pre-commit hooks by running:

   ```sh
   pre-commit install
   ```

   This command sets up the hooks to run automatically on `git commit` and `git push`.

## Linters and Formatters

Our project uses the following linters and formatters to ensure code quality:

- **Trailing Whitespace Checker**: Removes unnecessary trailing spaces.
- **End-of-File Fixer**: Ensures that files end with a newline.
- **YAML Checker**: Checks YAML files for syntax errors.
- **Large Files Checker**: Prevents accidentally adding large files to the Git repository.
- **JSON Checker**: Ensures JSON files are correctly formatted and syntactically correct.
- **Merge Conflict Checker**: Checks for unresolved merge conflicts.
- **Mypy**: A static type checker for Python, ensuring type consistency in your code.
- **Ruff**: An extremely fast Python linter and formatter, which automatically fixes certain issues.

### Running Linters Manually

Although the hooks will automatically run on commit and push, you can manually run all configured linters on all files with:

```sh
pre-commit run --all-files
```

To run a specific linter, use:

```sh
pre-commit run <hook-id> --all-files
```

Replace `<hook-id>` with the ID of the linter you wish to run (e.g., `ruff`, `mypy`).

## Contributing Your Changes

After making your changes, ensure all linters pass before committing:

1. Stage your changes:

   ```sh
   git add .
   ```

2. Commit your changes. The pre-commit hooks will run automatically:

   ```sh
   git commit -m "Your commit message"
   ```

   If any hooks report errors, fix the reported issues and try committing again.

3. Once all hooks pass, push your changes:

   ```sh
   git push origin <your-branch>
   ```

Replace `<your-branch>` with the name of your feature or bugfix branch.

## Pull Request

After pushing your changes, create a pull request through the GitHub interface. Our team will review your contribution and merge it if everything is in order.

